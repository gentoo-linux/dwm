#!/bin/sh

texters="󰬟 Xmind\n󰍔 Markdown"

texter=$(echo -e "$texters" | dmenu -i)

case "$texter" in
	"󰬟 Xmind" ) xmind ;;
	"󰍔 Markdown" ) marktext ;;
esac
