#!/bin/bash

ims="󰘑 WeChat\n󰘅 QQ\n󰭹 DingTalk"

im=$(echo -e "$ims" | dmenu -i)

case "$im" in
    "󰘑 WeChat" ) /usr/lib/wechat-universal/start.sh ;;
    "󰘅 QQ" ) LD_PRELOAD="/usr/lib64/libgnutls.so.30" qq ;;
    "󰭹 DingTalk" ) dingtalk ;;
esac
