#!/bin/sh

ides="󰬡 Zed\n󰨞 VsCode\n Idea"

ide=$(echo -e "$ides" | dmenu -noi -p " Choose one to begin:")

case "$ide" in
	"󰬡 Zed" ) zeditor ;;
	"󰨞 VsCode" ) GTK_THEME=Adwaita-dark code ;;
	" Idea" ) idea-ultimate ;;
esac
