#!/bin/sh

choices="󰈬 Word\n󰈛 Excel\n󰈧 PowerPoint"

choice=$(echo -e "$choices" | dmenu -i)

case "$choice" in
        "󰈬 Word" ) wps ;;
        "󰈛 Excel" ) et ;;
        "󰈧 PowerPoint" ) wpp ;;
esac
