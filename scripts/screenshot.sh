#!/bin/bash


# 检查软件是否安装
check_software_installed() {
  local software=$1
  if ! qlist -I | grep -q "$software"; then
    notify-send "$software 没有安装"
    exit 1
  fi
}

# 检查 xdotool 是否安装
check_software_installed "xdotool"

# 检查 flameshot 是否安装
check_software_installed "flameshot"

# 获取当前聚焦窗口的ID
window_id=$(xdotool getwindowfocus)

# 获取窗口的类名
window_classname=$(xdotool getwindowclassname $window_id)

# 获取当前时间（时分秒）
current_time=$(date +%H%M%S)

# 设置截图文件名
file_name="${window_classname}_${current_time}.png"

# 获取当前日期（年月日）
current_date=$(date +%Y%m%d)

# 设置截图保存路径
file_path="/home/ali/Pictures/screenshot/$current_date"

# 检查截图保存目录是否存在，如果不存在则创建
if [ ! -d "$file_path" ]; then
  mkdir -p "$file_path"
fi

# 启动Flameshot进行截图并指定保存路径和延迟时间
flameshot gui -p "${file_path}/${file_name}" -c -d 100

