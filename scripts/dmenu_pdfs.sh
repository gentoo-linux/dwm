#!/bin/sh

# FILE=$(find ~/Documents -name "*.pdf" | dmenu -i -l 10 -p "󰈦 :")

# if [ ! -z "$FILE" ]; then
#     zathura "$FILE"
# fi
# 创建映射表（关联数组），键为文件名，值为完整路径
declare -A FILE_MAP
while IFS= read -r -d '' file; do
    filename=$(basename "$file")
    FILE_MAP["$filename"]="$file"
done < <(find ~/Documents -name "*.pdf" -print0)

# 提取所有文件名作为 dmenu 的输入
FILES=$(printf "%s\n" "${!FILE_MAP[@]}")

# 使用 dmenu 显示文件名列表供选择
FILE=$(echo "$FILES" | dmenu -i -l 10 -p "󰈦 :")

# 检查 dmenu 是否返回了选择结果
if [ -n "$FILE" ]; then
    # 根据选定的文件名获取完整路径
    FULL_PATH="${FILE_MAP[$FILE]}"
    
    # 确认文件存在且是文件类型，然后尝试打开它
    if [ -n "$FULL_PATH" ] && [ -f "$FULL_PATH" ]; then
        # 使用 zathura 打开文件
        zathura "$FULL_PATH"
    else
        notify-send "错误：找不到文件或无法访问。" 
        exit 1
    fi
else
    notify-send "未选择任何文件。"
    exit 1
fi
