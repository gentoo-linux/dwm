#! /bin/bash
# DWM自启动脚本 仅作参考 

_thisdir=/home/ali/Downloads/dwm


daemons() {
    [ $1 ] && sleep $1
    /home/ali/Downloads/dwm/statusbar/statusbar.sh cron &   # 开启状态栏定时更新
    xfce4-power-manager &
    xss-lock -- slock &       # 开启自动锁屏程序
    # xautolock -time 3 -locker "slock" &        # slock启动
    fcitx5 &                                  # 开启输入法
    flameshot &                               # 截图要跑一个程序在后台 不然无法将截图保存到剪贴板
    dunst -conf /home/ali/Downloads/dwm/config/dunst.conf & # 开启通知server
    picom --experimental-backends --config /home/ali/Downloads/dwm/config/picom.conf >> /dev/null 2>&1 & # 开启picom
}


daemons 3 &                                   # 后台程序项
feh --bg-fill --randomize /home/ali/Pictures/wallpaper/* # 每300秒更新壁纸
